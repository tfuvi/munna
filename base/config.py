#!/usr/bin/env python
import pathlib

# For base modules.
BASE_DIR = pathlib.Path.cwd().parent
DATABASE_PATH = str((BASE_DIR / 'db.sqlite3'))
SMTP_PORT = 463
EMAIL_RE = r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"

# Regarding Printing
HELP_TEXT = """
 __  __
 |  \/  |_   _ _ __  _ __   __ _ 
 | |\/| | | | | '_ \| '_ \ / _` |
 | |  | | |_| | | | | | | | (_| |
 |_|  |_|\__,_|_| |_|_| |_|\__,_|
==================================
>Managing Mails With Discord Bot!! 

💡 How To Use:
    1. `-h`/`--help`: Show you this string.
    2. `create table`: Will Recreate the database table.
    3. `create user <usename> <password>`: Will create a new user.
    4. `add <email>`: Will add a new email to mailing list/

🛠️ :: Constructed by `Vivek Kumar`.
"""
PREFIX = "[MUNNA]::"

# Regarding Tests:
TEST_DATABASE_NAME = 'test.db.sqlite3'
TEST_DATABASE_PATH = str((BASE_DIR / TEST_DATABASE_NAME))
