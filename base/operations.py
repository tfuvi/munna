#!/usr/bin/env python

import hashlib
from .database import Mail, User
from . import validators


def hash_password(password):
   """
   Description: Return the hexify sha256 cipher.
   """
   hash = hashlib.sha256()
   hash.update(password.encode())
   return hash.hexdigest()

def create_mail(mail):
    """
    Add the given mail to the database after validating.
    """
    if validators.is_email(mail):
        mail = Mail(email=mail)
        mail.save()
        return True, "Mail is added to Mailing List."
    return False, "Email is not valid"
  
def create_user(email, password):
    """
    Save the giver username and password to the database.
    """
    if validators.is_email(email) == True:
        if is_user(email) == False:
            if validators.is_password_valid(password)[0]:
                hashed_password = hash_password(password)
                user = User(email=email, password=hashed_password)
                user.save()
                return True, "User created."
            else:
                return False, "Password is not valid."
        else:
            return False, "User already exsited"
    return False, "Email is not  valid."


def is_user(email, password=None):
    """
    check if user is already exist , in database or not

    :email <string>    : user email
    :password <string> : user password
    :return <boolean>  : return true if user instance is already exist, else false.
    """
    if password != None:
        old_hashed_password = hash_password(password)
        user_instance = User.select().where(User.email == email, User.password == old_hashed_password)
    else:
        user_instance = User.select().where(User.email == email)

    if user_instance.exists():
        return True
    else:
        return False

def is_mail(emai):
    """
    check if email is already exist, in database or not

    :mail <string>    : email
    :return <boolean> : return true if email instance is already exist, else false.
    """
    mail_instance = Mail.select().where(Mail.email == email)
    if mail_instance.exists():
        return True
    else:
        return False

def change_password(email, old_password, new_password):
    """
    update exsited user password

    :email <String> : user email
    :old_password <String> : old password of the user
    :new password <String> : new password 
    :return <Boolean> : return true if password update sucessfully, else false.
    """
    user_instance = is_user(email, old_password)
    if user_instance == True:
        new_hashed_password = hash_password(new_password)
        user = User.get(User.email == email)
        user.password = new_hashed_password
        user.save()
        return True
    else:
        return False








