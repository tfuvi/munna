#!/usr/bin/env python

import re
import string
from . import config


def is_email(mail):
    """
    Returns: True/False
    Description: Return True if email is validated otherwise False.
    """
    if re.search(config.EMAIL_RE, mail):
        return True
    return False


def is_password_valid(password):
    """
    Returns: (True/False, "message")
    Description: Returns True if the password is valid otherwaise False.
    """
    for whitespace in string.whitespace:
        if whitespace in password:
            return (False, "Password can't have white space character.")

    if password.isalnum():
        return (True, "Valid")
