#!/usr/bin/env python
"""
Name: Munna, Another Discord bot after `Kaleen` for our discord server.
Author: Vivek Kumar
"""
import os
import sys
from getpass import getpass
from base import config
from base import database
from base import operations
from base import validators
from discord.ext.commands import Bot

if __name__ == "__main__":
    # Command: ./main.py -h/--help
    if len(sys.argv) > 1:
        if sys.argv[1] == '-h' or sys.argv[1] == '--help':
            print(config.HELP_TEXT)

    # Command: ./main.py create tables
    else if len(sys.argv) > 2:
        if sys.argv[1] == "create":
            db = database.db
            db.connect()

            if and sys.argv[2] == "tables":
                db.create_tables([database.Mail, database.User])
                print(f"{config.PREFIX}Tables Mail and User are Created.")

            else if sys.argv[2] == "user":
                email = input(f"{config.PREFIX} Enter email: ")
                password = getpass(f"{config.PREFIX} Enter password: ")
                result = operations.create_user(email, password)
                print(f"{config.PREFIX} {result[1]}")

            else if sys.argv[3] == "mail":
                email = input(f"{config.PREFIX} Enter mail: ")
                result = operations.create_mail(email)
                print(f"{config.PREFIX} {result[1]}")

                




